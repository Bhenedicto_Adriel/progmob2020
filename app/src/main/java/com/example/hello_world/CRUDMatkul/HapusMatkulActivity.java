package com.example.hello_world.CRUDMatkul;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hello_world.CRUDdosen.HapusDosenActivity;
import com.example.hello_world.CRUDdosen.MainDosenActivity;
import com.example.hello_world.Network.GetDataService;
import com.example.hello_world.Network.RetrofitClientInstance;
import com.example.hello_world.R;
import com.example.hello_world.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapusMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_matkul);

        EditText kode = (EditText)findViewById(R.id.delkodemk);
        Button btndel = (Button) findViewById(R.id.btndel);
        pd = new ProgressDialog(HapusMatkulActivity.this);

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mtkl(
                        kode.getText().toString(),"72180225"
                );


                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this, "Data Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(HapusMatkulActivity.this, MainMatkulActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMatkulActivity.this, "ERROR", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}