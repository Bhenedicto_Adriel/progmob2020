package com.example.hello_world.CRUDMatkul;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hello_world.CRUDdosen.DosenAddActivity;
import com.example.hello_world.CRUDdosen.DosenGetAllActivity;
import com.example.hello_world.CRUDdosen.HapusDosenActivity;
import com.example.hello_world.CRUDdosen.MainDosenActivity;
import com.example.hello_world.Pertemuan6.MainMenuActivity;
import com.example.hello_world.R;

public class MainMatkulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        Button btnadd = (Button)findViewById(R.id.btnaddmatkul);
        Button btnlihat = (Button)findViewById(R.id.btnlihatmatkul);
        Button btndel = (Button)findViewById(R.id.btndeletematkul);
        Button btnupdate = (Button)findViewById(R.id.btnupdatematkul);
        Button btnMenu = (Button)findViewById(R.id.btnMenu);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulAddActivity.class);
                startActivity(intent);
            }
        });
        btnlihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, HapusMatkulActivity.class);
                startActivity(intent);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });


    }
}