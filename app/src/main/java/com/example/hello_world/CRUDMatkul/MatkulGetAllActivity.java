package com.example.hello_world.CRUDMatkul;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.hello_world.CRUDdosen.DosenGetAllActivity;
import com.example.hello_world.Network.GetDataService;
import com.example.hello_world.Network.RetrofitClientInstance;
import com.example.hello_world.R;
import com.example.hello_world.adapter.DosenRecyclerAdapter;
import com.example.hello_world.adapter.MatakuliahRecyclerAdapter;
import com.example.hello_world.model.Dosen;
import com.example.hello_world.model.Matakuliah;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulGetAllActivity extends AppCompatActivity {
    RecyclerView rvMtkl;
    MatakuliahRecyclerAdapter mtklAdapter;
    ProgressDialog pd;
    List<Matakuliah> matakuliahList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMtkl = (RecyclerView)findViewById(R.id.rvMatakuliah);
        pd = new ProgressDialog(this);
        pd.setTitle("Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matakuliah>> call = service.getMatkul("72180225");

        call.enqueue(new Callback<List<Matakuliah>>() {
            @Override
            public void onResponse(Call<List<Matakuliah>> call, Response<List<Matakuliah>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                mtklAdapter = new MatakuliahRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulGetAllActivity.this);
                rvMtkl.setLayoutManager(layoutManager);
                rvMtkl.setAdapter(mtklAdapter);

            }

            @Override
            public void onFailure(Call<List<Matakuliah>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MatkulGetAllActivity.this,"error",Toast.LENGTH_LONG);
            }
        });
    }
}