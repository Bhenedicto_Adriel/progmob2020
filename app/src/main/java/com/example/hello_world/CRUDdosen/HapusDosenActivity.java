package com.example.hello_world.CRUDdosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hello_world.Crud.HapusMhsActivity;
import com.example.hello_world.Crud.MainMhsActivity;
import com.example.hello_world.Network.GetDataService;
import com.example.hello_world.Network.RetrofitClientInstance;
import com.example.hello_world.R;
import com.example.hello_world.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapusDosenActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_dosen);

        EditText nidn = (EditText)findViewById(R.id.delnidn);
        Button btndel = (Button) findViewById(R.id.btndel);
        pd = new ProgressDialog(HapusDosenActivity.this);

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dsn(
                        nidn.getText().toString(),"72180225"
                );


                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this, "Data Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(HapusDosenActivity.this, MainDosenActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this, "ERROR", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}