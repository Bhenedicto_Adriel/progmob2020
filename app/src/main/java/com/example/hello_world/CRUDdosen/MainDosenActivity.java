package com.example.hello_world.CRUDdosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hello_world.CRUDMatkul.MainMatkulActivity;
import com.example.hello_world.Crud.HapusMhsActivity;
import com.example.hello_world.Crud.MahasiswaAddActivity;
import com.example.hello_world.Crud.MahasiswaGetAllActivity;
import com.example.hello_world.Crud.MainMhsActivity;
import com.example.hello_world.Pertemuan6.MainMenuActivity;
import com.example.hello_world.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnadd = (Button)findViewById(R.id.btnadddsn);
        Button btnlihat = (Button)findViewById(R.id.btnlihatdsn);
        Button btndel = (Button)findViewById(R.id.btndeletedsn);
        Button btnupdate = (Button)findViewById(R.id.btnupdatedsn);
        Button btnMenu = (Button)findViewById(R.id.btnMenu);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
        btnlihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, HapusDosenActivity.class);
                startActivity(intent);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
    }
}