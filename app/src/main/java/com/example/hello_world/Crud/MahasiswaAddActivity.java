package com.example.hello_world.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hello_world.Network.GetDataService;
import com.example.hello_world.Network.RetrofitClientInstance;
import com.example.hello_world.R;
import com.example.hello_world.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        final EditText ednama = (EditText)findViewById(R.id.editNama);
        final EditText ednim = (EditText)findViewById(R.id.editNim);
        final EditText edalamat = (EditText)findViewById(R.id.editAlamat);
        final EditText edemail = (EditText)findViewById(R.id.editEmail);
        Button btnsimpan = (Button)findViewById(R.id.btnUpdatedata);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        ednama.getText().toString(),
                        ednim.getText().toString(),
                        edalamat.getText().toString(),
                        edemail.getText().toString(),"","72180225"
                );


                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "Data Berhasil di Simpan", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MahasiswaAddActivity.this, MainMhsActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "ERROR", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}