package com.example.hello_world.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hello_world.Network.GetDataService;
import com.example.hello_world.Network.RetrofitClientInstance;
import com.example.hello_world.R;
import com.example.hello_world.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        EditText ednimlama = (EditText)findViewById(R.id.editnimlama);
        EditText ednama = (EditText)findViewById(R.id.editNama);
        EditText ednim = (EditText)findViewById(R.id.editNim);
        EditText edalamat = (EditText)findViewById(R.id.editAlamat);
        EditText edemail = (EditText)findViewById(R.id.editEmail);
        Button btnedit = (Button)findViewById(R.id.btnUpdatedata);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null){
            ednama.setText(data.getStringExtra("nama"));
            ednim.setText(data.getStringExtra("nim"));
            ednimlama.setText(data.getStringExtra("nim"));
            edalamat.setText(data.getStringExtra("alamat"));
            edemail.setText(data.getStringExtra("email"));
        }

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        ednama.getText().toString(),
                        ednim.getText().toString(),
                        ednimlama.getText().toString(),
                        edalamat.getText().toString(),
                        edemail.getText().toString(),
                        "Kosongkan saja",
                        "72180225"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Tidak Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}