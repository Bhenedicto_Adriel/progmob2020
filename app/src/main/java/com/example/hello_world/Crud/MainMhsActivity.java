package com.example.hello_world.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.hello_world.CRUDMatkul.MainMatkulActivity;
import com.example.hello_world.MainActivity;
import com.example.hello_world.Pertemuan6.MainMenuActivity;
import com.example.hello_world.R;
import com.example.hello_world.pertemuan2.ListActivity;

public class MainMhsActivity extends AppCompatActivity {
    EditText nim,nama,alamat,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnadd = (Button)findViewById(R.id.btnaddmhs);
        Button btnlihat = (Button)findViewById(R.id.btnlihatmhs);
        Button btndel = (Button)findViewById(R.id.btndeletemhs);
        Button btnupdate = (Button)findViewById(R.id.btnupdatemhs);
        Button btnMenu = (Button)findViewById(R.id.btnMenu);

//        if(savedInstanceState ==null){
//            Intent data = getIntent();
////            btnadd.setVisibility(View.GONE);
////            btnlihat.setVisibility(View.GONE);
////            btnupdate.setVisibility(View.VISIBLE);
////            nim.setText(data.getStringExtra("nim"));
////            nama.setText(data.getStringExtra("nama"));
////            alamat.setText(data.getStringExtra("alamat"));
////            email.setText(data.getStringExtra("email"));
//        }
//


        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(intent);
            }
        });
        btnlihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intent);
            }
        });

        btndel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, HapusMhsActivity.class);
                startActivity(intent);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });
    }
}