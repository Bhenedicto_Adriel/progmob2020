package com.example.hello_world;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        TextView txthelp = (TextView)findViewById(R.id.help1);

        Bundle b = getIntent().getExtras();
        String help1 = b.getString("help_string");
        txthelp.setText(help1);
    }
}