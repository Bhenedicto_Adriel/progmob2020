package com.example.hello_world;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.hello_world.Crud.MahasiswaGetAllActivity;
import com.example.hello_world.pertemuan2.CardActivity;
import com.example.hello_world.pertemuan2.ListActivity;
import com.example.hello_world.pertemuan2.RecylerActivity;
import com.example.hello_world.pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    public static String EXTRA_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //variable
        final TextView textView = (TextView)findViewById(R.id.mainActivityTextView);
        Button mybtn = (Button)findViewById(R.id.btn1);
        Button mybtnhelp = (Button)findViewById(R.id.help1);
        final EditText myedit = (EditText)findViewById(R.id.edit1);

        Button btnlist = (Button)findViewById(R.id.btnlistview);
        Button btnrecycler = (Button)findViewById(R.id.btnrecyclerview);
        Button btncard = (Button)findViewById(R.id.btncardview);
        Button btnpertemuan = (Button)findViewById(R.id.btnpertemuan);


        //action
        textView.setText(R.string.hello_world);
        mybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("click sini ", myedit.getText().toString());
                textView.setText(myedit.getText().toString());
            }
        });

        mybtnhelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,HelpActivity.class);
                Bundle b = new Bundle();

                b.putString("help_string", myedit.getText().toString());
                intent.putExtras(b);

                startActivity(intent);
            }
        });

        btnlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        btnrecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RecylerActivity.class);
                startActivity(intent);
            }
        });

        btncard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CardActivity.class);
                startActivity(intent);
            }
        });


        btnpertemuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });
    }

    public void Tracker(View view){
        Intent intent = new Intent(MainActivity.this,tracker_activity.class);
        startActivity(intent);
    }

//    public void sendMessage(View view) {
//        Intent intent = new Intent(this, DisplayMessageActivity.class);
//        EditText editText = (EditText) findViewById(R.id.edit1);
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
//        startActivity(intent);
//    }

}