package com.example.hello_world.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hello_world.CRUDMatkul.MainMatkulActivity;
import com.example.hello_world.CRUDdosen.DosenGetAllActivity;
import com.example.hello_world.CRUDdosen.MainDosenActivity;
import com.example.hello_world.Crud.MainMhsActivity;
import com.example.hello_world.R;

public class ListMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_main);

        Button btndsn = (Button)findViewById(R.id.btndatadsn);
        Button btnmhs = (Button)findViewById(R.id.btndatamhs);
        Button btnmk = (Button)findViewById(R.id.btndatamatkul);

        btndsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMainActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });

        btnmhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMainActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        btnmk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListMainActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}