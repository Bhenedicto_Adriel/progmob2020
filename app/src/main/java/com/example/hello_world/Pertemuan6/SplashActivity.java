package com.example.hello_world.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.example.hello_world.CRUDMatkul.MainMatkulActivity;
import com.example.hello_world.CRUDdosen.MainDosenActivity;
import com.example.hello_world.Crud.MainMhsActivity;
import com.example.hello_world.MainActivity;
import com.example.hello_world.R;
import com.example.hello_world.model.User;

import java.util.List;

public class SplashActivity extends AppCompatActivity {
    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();

//        session = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
//        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
//            finish();
//            startActivity(new Intent(SplashActivity.this, MainMenuActivity.class));
//            return;
//        }

        Thread thread = new Thread(){
            public void run(){
                try{
                    sleep(4000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        thread.start();
    }
}