package com.example.hello_world.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hello_world.CRUDdosen.DosenUpdateActivity;
import com.example.hello_world.Crud.MahasiswaUpdateActivity;
import com.example.hello_world.R;
import com.example.hello_world.model.Dosen;
import com.example.hello_world.model.Mahasiswa;

import java.util.ArrayList;
import java.util.List;

public class DosenRecyclerAdapter extends RecyclerView.Adapter<DosenRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Dosen> dosenList;


    public DosenRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenRecyclerAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }


    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card_dsn,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);

        holder.tvnama.setText(d.getNama());
        holder.tvnidn.setText(d.getNidn());
        holder.tvalamat.setText(d.getAlamat());
        holder.tvemail.setText(d.getEmail());
        holder.tvgelar.setText(d.getGelar());
        holder.d = d;
    }


    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvnama,tvnidn,tvemail,tvalamat,tvgelar;
        private RecyclerView rvGetAllDosen;
        Dosen d;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvnama = itemView.findViewById(R.id.tvnamadosen);
            tvnidn = itemView.findViewById(R.id.tvnidn);
            tvalamat = itemView.findViewById(R.id.tvalamatdosen);
            tvemail = itemView.findViewById(R.id.tvemaildosen);
            tvgelar = itemView.findViewById(R.id.tvgelardosen);
            rvGetAllDosen = itemView.findViewById(R.id.rvGetAllDosen);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), DosenUpdateActivity.class);
                    intent.putExtra("nidn",d.getNidn());
                    intent.putExtra("nama",d.getNama());
                    intent.putExtra("alamat",d.getAlamat());
                    intent.putExtra("email",d.getEmail());
                    intent.putExtra("gelar",d.getGelar());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
