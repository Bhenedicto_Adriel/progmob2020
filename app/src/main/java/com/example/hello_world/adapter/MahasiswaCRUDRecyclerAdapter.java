package com.example.hello_world.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hello_world.Crud.MahasiswaUpdateActivity;
import com.example.hello_world.Crud.MainMhsActivity;
import com.example.hello_world.R;
import com.example.hello_world.model.Mahasiswa;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Mahasiswa> mahasiswaList;


    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }


    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.tvnama.setText(m.getNama());
        holder.tvnim.setText(m.getNim());
        holder.tvalamat.setText(m.getAlamat());
        holder.tvemail.setText(m.getEmail());
        holder.m = m;

//        holder.tvnohp.setText(m.getNohp());
    }


    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvnama,tvnim,tvemail,tvalamat,tvnohp;
        private RecyclerView rvGetMhsAll;
        Mahasiswa m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvnama = itemView.findViewById(R.id.tvnama);
            tvnim = itemView.findViewById(R.id.tvnim);
            tvalamat = itemView.findViewById(R.id.tvalamat);
            tvemail = itemView.findViewById(R.id.tvemail);
//            tvnohp = itemView.findViewById(R.id.tvnohp);
            rvGetMhsAll = itemView.findViewById(R.id.rvGetMhsAll);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), MahasiswaUpdateActivity.class);
                    intent.putExtra("nim",m.getNim());
                    intent.putExtra("nama",m.getNama());
                    intent.putExtra("alamat",m.getAlamat());
                    intent.putExtra("email",m.getEmail());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
