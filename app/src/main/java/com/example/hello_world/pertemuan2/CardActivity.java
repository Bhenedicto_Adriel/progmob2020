package com.example.hello_world.pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.hello_world.R;
import com.example.hello_world.adapter.MahasiswaCardAdapter;
import com.example.hello_world.adapter.MahasiswaRecyclerAdapter;
import com.example.hello_world.model.Mahasiswa;

import java.util.ArrayList;
import java.util.List;

public class CardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

            RecyclerView rv = (RecyclerView)findViewById(R.id.rvcardview);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        List<Mahasiswa> mahasiswaList = new ArrayList<>();

        Mahasiswa m1 = new Mahasiswa("Dita","72180225","6969696969");
        Mahasiswa m2 = new Mahasiswa("ujang","72115151","000000000");
        Mahasiswa m3 = new Mahasiswa("supri","72182255","444444444");
        Mahasiswa m4 = new Mahasiswa("kikil","72182305","777777676");
        Mahasiswa m5 = new Mahasiswa("cimol","72182064","999999999");
        Mahasiswa m6 = new Mahasiswa("udin","72180299","66666666");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        mahasiswaList.add(m6);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(CardActivity.this));
        rv.setAdapter(mahasiswaCardAdapter);
    }
}